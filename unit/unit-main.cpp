// Guard unit test runner program
// This file is part of CBOR-lite which is copyright Isode Limited
// and others and released under a MIT license. For details, see the
// COPYRIGHT.md file in the top-level folder of the CBOR-lite software
// distribution.
#define BOOST_TEST_MAIN Guard
#include <boost/test/unit_test.hpp>
